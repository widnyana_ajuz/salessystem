<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		// Load form helper library
		$this->load->helper('form');
		// Load form validation library
		$this->load->library('form_validation');
		// Load session library
		$this->load->library('session');
		// Load database
		$this->load->model('m_user');
	}

	public function index()
	{
		$this->load->view('index');
	}

	public function user_login_process() {
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE) {
			if(isset($this->session->userdata['logged_in'])){
				// redirect(base_url().'penjualan');
				echo "login";
			}else{
				echo "fuck";//$this->load->view('index');
			}
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
			$result = $this->m_user->loginDataUser($username, $password);

			if ($result == TRUE) {
				$username = $this->input->post('username');
				
				$session_data = array(
					'username' => $username,
				);
				
				$this->session->set_userdata('logged_in', $session_data);
				// redirect(base_url().'penjualan');
				header("location: ".base_url()."penjualan");
			} else {
				$data = array(
					'error_message' => 'Invalid Username or Password'
				);
				$this->load->view('index', $data);
			}
		}
	}

	public function logout() {
		// Removing session data
		$sess_array = array(
			'username' => ''
		);

		$this->session->unset_userdata('logged_in', $sess_array);
		// $data['message_display'] = 'Successfully Logout';
		header("location: ".base_url());
	}
}
