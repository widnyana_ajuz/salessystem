<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterbarang extends CI_Controller {

	function __construct(){
        parent:: __construct();
        $this->load->library('session');
        $this->load->model("m_masterbarang");
    }

	public function index()
	{
		$data['user'] = "";
		$data['databarang'] = $this->m_masterbarang->getDataBarang();
		$this->load->view('base/header', $data);
		$this->load->view('masterbarang', $data);
	}

	public function searchDataBarang(){
		$keyword =  $_POST['keyword'];
		$result = $this->m_masterbarang->getSearchDataBarang($keyword);
		echo json_encode($result);
	}

	public function addDataBarang(){
		$data = array(
			'nama_barang' => $_POST['nama_barang'],
			'satuan' => $_POST['satuan'],
			'harga_jual' => $_POST['harga_jual'],
			'satuan2' => $_POST['satuan2'],
			'harga_jual2' => $_POST['harga_jual2']
		);

		$insert = $this->m_masterbarang->insertDatabarang($data);
		$result['message'] = "Data Sukses Ditambahkan";
		echo json_encode($result);
	}

	public function editDataBarang(){
		$data = array(
			'nama_barang' => $_POST['nama'],
			'satuan' => $_POST['satuan'],
			'harga_jual' => $_POST['hj'],
			'satuan2' => $_POST['satuan2'],
			'harga_jual2' => $_POST['hj2']
		);

		$id = $_POST['id'];

		$update = $this->m_masterbarang->updateDataBarang($data, $id);
		$result['message'] = "Data Sukses DiPerbarui";
		echo json_encode($result);
	}

	public function deleteDataBarang(){
		$id =  $_POST['id_barang'];
		$delete = $this->m_masterbarang->deleteDataBarang($id);
		$result['message'] = "Data Sukses Dihapus";
		echo json_encode($result);
	}
}
