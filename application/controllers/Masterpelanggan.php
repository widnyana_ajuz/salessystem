<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterpelanggan extends CI_Controller {

	function __construct(){
        parent:: __construct();
        $this->load->library('session');
        $this->load->model("m_masterpelanggan");
    }

	public function index()
	{
		$data['user'] = "";
		$data['datapelanggan'] = $this->m_masterpelanggan->getDataPelanggan();
		$this->load->view('base/header', $data);
		$this->load->view('masterpelanggan', $data);
	}

	public function searchDataPelanggan(){
		$keyword =  $_POST['keyword'];
		$result = $this->m_masterpelanggan->getSearchDataPelanggan($keyword);
		echo json_encode($result);
	}

	public function addDataPelanggan(){
		$data = array(
			'nama' => $_POST['nama'],
			'status' => $_POST['status'],
			'telepon' => $_POST['telepon'],
			'alamat' => $_POST['alamat']
		);

		$insert = $this->m_masterpelanggan->insertDataPelanggan($data);
		$result['message'] = "Data Sukses Ditambahkan";
		echo json_encode($result);
	}

	public function editDataPelanggan(){
		$data = array(
			'nama' => $_POST['nama'],
			'status' => $_POST['status'],
			'telepon' => $_POST['telepon'],
			'alamat' => $_POST['alamat']
		);

		$id = $_POST['id'];

		$update = $this->m_masterpelanggan->updateDataPelanggan($data, $id);
		$result['message'] = "Data Sukses Diperbarui";
		echo json_encode($result);
	}

	public function deleteDataPelanggan(){
		$id =  $_POST['id_pelanggan'];
		$delete = $this->m_masterpelanggan->deleteDataPelanggan($id);
		$result['message'] = "Data Sukses Dihapus";
		echo json_encode($result);
	}

}
