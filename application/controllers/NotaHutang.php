<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NotaHutang extends CI_Controller {

	function __construct(){
        parent:: __construct();
        $this->load->library('session');
        $this->load->model("m_notahutang");
    }

  public function index()
	{
		$data['user'] = "";
		$this->load->view('base/header', $data);
		$this->load->view('notahutang', $data);
	}

	public function selectNotaPenjualan(){
		$dari = $_POST['dari'];
		$sampai = $_POST['sampai'];
		$result = $this->m_notahutang->selectNotaPenjualan($dari, $sampai);
		echo json_encode($result);
	}

	public function selectCetakPenjualan(){
		$dari = $_POST['dari'];
		$sampai = $_POST['sampai'];
		$result = $this->m_notahutang->selectCetakPenjualan($dari, $sampai);
		echo json_encode($result);
	}

	public function selectDataNotaId(){
		$id = $_POST['no_nota'];
		$result = $this->m_notahutang->selectDataNotaId($id);
		echo json_encode($result);
	}

	public function cetakPenjualan(){
		$this->load->view('notapenjualan', $data);
	}

}
?>
