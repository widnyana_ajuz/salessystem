<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

	function __construct(){
        parent:: __construct();
        $this->load->library('session');
        $this->load->model("m_penjualan");
    }
	public function index()
	{
		$data['user'] = "";//['no_nota']
		$last = $this->m_penjualan->getNomerNota()['no_nota'];
		$new = $this->checkNomorNota(intval($last));
		$data['no_nota'] = $new;
		$this->load->view('base/header', $data);
		$this->load->view('penjualan');
	}

	public function checkNomorNota($no){
		$no++;
		if($no<10)
			return "00".$no;
		if($no<100)
			return "0".$no;
		else
			return $no;
	}

	public function getDataBarangId(){
		$id = $_POST['id'];
		$result = $this->m_penjualan->getDataBarangId($id);

		echo json_encode($result);
	}

	public function submitDataBarang(){
		$id = $_POST['id'];
		$satuan = $_POST['satuan'];
		$result = $this->m_penjualan->getDataBarangId($id);

		if(!empty($result)){
			if($satuan==1){
				$data = array(
					'nomor_nota' => $_POST['no_nota'],
					'nama_barang' => $result['nama_barang'],
					'satuan' => $result['satuan'],
					'harga_beli' => $result['harga_beli'],
					'harga_jual' => $result['harga_jual'],
					'qty' => $_POST['qty']
				);
			}else{
				$data = array(
					'nomor_nota' => $_POST['no_nota'],
					'nama_barang' => $result['nama_barang'],
					'satuan' => $result['satuan2'],
					'harga_jual' => $result['harga_jual2'],
					'qty' => $_POST['qty']
				);
			}

			$insert = $this->m_penjualan->insertDatabarang($data);
		}else{
			$result['message'] = "Data Barang Tidak Ditemukan";
		}

		echo json_encode($result);
	}

	public function submitNotaPenjualan(){
		//2016-08-14 07:23:26
		$today = date("Y-m-d H:i:s");
		$status = "";

		if(intval($_POST['total_bayar'])>intval($_POST['pembayaran']))
			$status = "BON";
		else
			$status = "LUNAS";

		$data = array(
			'no_nota' => $_POST['no_nota'],
			'id_pelanggan' => $_POST['id_pelanggan'],
			'waktu' => $_POST['waktu'],
			'total_bayar' => $_POST['total_bayar'],
			'pembayaran' => $_POST['pembayaran'],
			'status' => $status,
		);

		$insert = $this->m_penjualan->insertNotaPenjualan($data);
		$result['message'] = "Data Sukses Ditambahkan";
		echo json_encode($result);
	}

	public function editDataBarang(){
		$id = $_POST['id'];

		$data = array(
			'qty' => $_POST['qty']
		);

		$update = $this->m_penjualan->updateDataBarang($data, $id);
		$result['message'] = "Data Sukses DiPerbarui";
		echo json_encode($result);
	}

	public function searchDataBarang(){
		$no_nota = $_POST['no_nota'];
		$result = $this->m_penjualan->getSearchDataBarang($no_nota);
		echo json_encode($result);
	}

	public function deleteDataBarang(){
		$id =  $_POST['id_barang'];
		$delete = $this->m_penjualan->deleteDataBarang($id);
		$result['message'] = "Data Sukses Dihapus";
		echo json_encode($result);
	}

	public function deleteAllBarang(){
		$id =  $_POST['no_nota'];
		$delete = $this->m_penjualan->deleteAllBarang($id);
		$result['message'] = "Data Sukses Dihapus";
		echo json_encode($result);
	}

	public function getSatuanBarang(){
		$id =  $_POST['id'];
		$result = $this->m_penjualan->getSatuanBarang($id);
		echo json_encode($result);
	}

	public function getHutangPelanggan(){
		$id = $_POST['id'];
		$result = $this->m_penjualan->getHutangPelanggan($id);
		echo json_encode($result);
	}

	public function SetBayarHutang(){
		$id = $_POST['id_pelanggan'];
		$waktu = $_POST['waktu'];
		$result = $this->m_penjualan->SetBayarHutang($id, $waktu);
		echo json_encode($result);
	}
}
