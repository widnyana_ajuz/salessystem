<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PrintCetak extends CI_Controller {

  function __construct(){
        parent:: __construct();
        $this->load->library('session');
        $this->load->model("m_penjualan");
        $this->load->model("m_notapenjualan");
        // $this->load->library('TableText');
        // if ( ! class_exists('TabelText'))
        // {
        //     require_once(APPPATH.'models/TabelText'.EXT);
        // }
  }

  public function printDotMatrix(){
    // require_once 'TableText.php';
  
    $tmpdir = sys_get_temp_dir();   # ambil direktori temporary untuk simpan file.
    $file =  tempnam($tmpdir, 'ctk');  # nama file temporary yang akan dicetak
    $handle = fopen($file, 'w');
    
    $id = $_POST['no_nota'];
    $pembayaran = 0;
    $kembalian = 0;
    $result = $this->m_notapenjualan->selectDataNotaId($id);

    $Data = "";
    $total = 0;
    $Data .= $this->printLine(8);
    $Data .= "Sistem Toko A ";
    $Data .= $this->printLine(8);
    $Data .= "\r\n------------------------------\r\n";
    $Data .= "No Nota   : ".$id."\r\n";
    $Data .= "Pelanggan : ".$_POST['pelanggan']."\r\n";
    $Data .= "Waktu     : ".$_POST['waktu'];
    $Data .= "\r\n------------------------------\r\n";
    $Data .= "Qty\tNama Barang\tHarga\tJumlah\r\n";

    foreach ($result as $value) {
      $Data .= $value['qty'];
      if($value['qty']>9)
        $Data .= "  ";
      else
        $Data .= "   ";
      $Data .= "\t".substr($value['nama_barang'], 0, 15)."\t";
      $Data .= $value['harga_jual']."\t";
      $Data .= intval($value['qty'])*intval($value['harga_jual'])."\t";
      $total += intval($value['qty'])*intval($value['harga_jual'])."\r\n";
    }
    $Data .= "\r\n------------------------------\r\n";
    $Data .= "Total\t\t\t\t";
    $Data .= $total."\n";
    $Data .= "\r\nBayar\t\t\t\t";
    $Data .= $pembayaran."\n";
    $Data .= "\r\nKembalian\t\t\t";
    $Data .= $kembalian."\n";

    echo $Data;
    // $Data  = $initialized;
    // $Data .= $condensed1;
    // $Data .= "==========================\n";
    // $Data .= "|     WIDNYANA SANTIKA      |\n";
    // $Data .= "==========================\n";
    // $Data .= "Ofidz Majezty is here\n";
    // $Data .= "We Love PHP Indonesia\n";
    // $Data .= "We Love PHP Indonesia\n";
    // $Data .= "We Love PHP Indonesia\n";
    // $Data .= "We Love PHP Indonesia\n";
    // $Data .= "We Love PHP Indonesia\n";
    // $Data .= "--------------------------\n";
    // echo($Data);
    fwrite($handle, $Data);
    fclose($handle);
    $hasil = copy($file, 'C:\xampp\ctk.txt');  # Lakukan cetak
    unlink($file);

    if($hasil)
      $hasil['message'] = "Cetak Berhasil";
    else
      $hasil['message'] = "Cetak Gagal";

    echo json_encode($hasil);
  }

  public function printLine($total){
    $line = "";
    for($i = 0; $i<$total; $i++){
      $line .= "-";
    }
    return $line."";
  }

  public function printHeader(){
    $header = "";
    for($i = 0; $i<$total; $i++){
      $line .= " ";
    }
  }

  public function getDataNota(){

  }
}
?>
