<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NotaPenjualan extends CI_Controller {

	function __construct(){
        parent:: __construct();
        $this->load->library('session');
        $this->load->model("m_notapenjualan");
    }

  public function index()
	{
		$data['user'] = "";
		$this->load->view('base/header', $data);
		$this->load->view('notapenjualan', $data);
	}

	public function selectNotaPenjualan(){
		$dari = $_POST['dari'];
		$sampai = $_POST['sampai'];
		$result = $this->m_notapenjualan->selectNotaPenjualan($dari, $sampai);
		echo json_encode($result);
	}

	public function selectCetakPenjualan(){
		$dari = $_POST['dari'];
		$sampai = $_POST['sampai'];
		$result = $this->m_notapenjualan->selectCetakPenjualan($dari, $sampai);
		echo json_encode($result);
	}

	public function selectDataNotaId(){
		$id = $_POST['no_nota'];
		$result = $this->m_notapenjualan->selectDataNotaId($id);
		echo json_encode($result);
	}

	public function cetakPenjualan(){
		$this->load->view('notapenjualan', $data);
	}

}
?>
