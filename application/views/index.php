<!DOCTYPE html>
<?php
	if (isset($this->session->userdata['logged_in'])) {
		header("location: ".base_url()."penjualan");
	}
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Forms</title>

<link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/datepicker3.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/styles.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>

	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Log in</div>
				<div class="panel-body">
					<!-- <form action="<?=base_url()?>home/login" role="form"> -->
					<?php echo form_open('home/user_login_process'); ?>
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Username" name="username" type="text" autofocus="" required="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" value="" required>
							</div>
							<div class="form-group col-md-3 ">
								<label>Printer:</label>
							</div>
							<div class="form-group col-md-3 ">
								<input type="radio" id="printer1" name="printer" value="printer1" required> DotPrix<!--dotprix-->
							</div>
							<div class="form-group col-md-3 ">
								<input type="radio" id="printer2" name="printer" value="printer2" required> Epson<!--//printer epson-->
							</div>
							<button type="submit" class="btn btn-primary button-full">Login</button>
						</fieldset>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->

	<script src="<?=base_url()?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/js/chart.min.js"></script>
	<script src="<?=base_url()?>assets/js/chart-data.js"></script>
	<script src="<?=base_url()?>assets/js/easypiechart.js"></script>
	<script src="<?=base_url()?>assets/js/easypiechart-data.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap-datepicker.js"></script>
	<script>
		$(document).ready(function(){
			$('#printer1').click(function(){
				if($('#printer1').is(':checked')) { localStorage.setItem("printer", "printer1") }
			})
			$('#printer2').click(function(){
				if($('#printer2').is(':checked')) { localStorage.setItem("printer", "printer2") }
			})
		})

		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){
				$(this).find('em:first').toggleClass("glyphicon-minus");
			});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>
</body>

</html>
