	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading" style="height:auto;">Nota Hutang<br/><span id="tanggalcetak" style="font-size:18px; margin-top:-20px; display:none;"></span></div>
					<div class="panel-body">
						<div class="col-md-12">
							<form role="form" id="searchPenjualan">
								<div class="col-md-12 no-padding">
									<div class="form-group col-md-3 ">
										<label>Tanggal Dari</label>
										<div class="input-group date" data-provide="datepicker">
										    <input id="tanggal_dari" type="text" class="form-control" required readonly="">
										    <div class="input-group-addon">
										        <span class="glyphicon glyphicon-th"></span>
										    </div>
										</div>
									</div>
									<div class="form-group col-md-3 no-padding">
										<label>Tanggal Sampai</label>
										<div class="input-group date" data-provide="datepicker">
										    <input id="tanggal_sampai" type="text" class="form-control" readonly="">
										    <div class="input-group-addon">
										        <span class="glyphicon glyphicon-th"></span>
										    </div>
										</div>
									</div>
									<div class="form-group col-md-1">
										<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
										<button type="submit" class="btn btn-info">Cari</button>
									</div>
								</div>
							</form>
							<hr>
							<div class="col-md-12 border-top">
								<div class="form-group col-md-2 no-padding-left">
									<button class="btn btn-info button-full" id="printData">Print Data</button>
								</div>
								<div class="form-group col-md-7">
								</div>
								<div class="form-group col-md-3 no-padding">
									<form id="submitSearch"><input type="text" id="inputSearch" class="form-control" placeholder="Search Penjualan"></form>
								</div>
								<table class="table table-bordered table-hover" id="tabelAsli">
								    <thead>
									    <tr>
									    	<th data-align="right" width="3%">No.</th>
									    	<th data-field="name" width="10%">Nota</th>
									        <th data-field="satuan" data-align="right%" width="15%">Waktu</th>
									        <th data-field="qty" width="25%">Nama Pelanggan</th>
													<th data-field="price" width="15%">Total Pembayaran</th>
									        <th data-field="price" width="15%">Nominal Hutang</th>
									        <th data-field="price" width="6%">Action</th>
									    </tr>

								    </thead>
								    <tbody id="tbody_penjualan">
								    	<?php
								    		echo '
								    			<tr><td colspan="6"><center>Tidak Tersedia Data</center></td></tr>
								    		';
								    	?>
								    </tbody>
								</table>

								<table class="table table-bordered table-hover" id="cetakTabel" style="display: none;">
									<thead>
										<tr>
											<th data-align="right" width="5%">No Nota</th>
											<th data-field="satuan" data-align="right%" width="15%">Waktu</th>
											<th data-field="qty" width="20%">Nama Pelanggan</th>
											<th data-field="name" width="20%">Nama Barang</th>
											<th data-field="qty" width="10%">Qty</th>
											<th data-field="price" width="15%">Harga</th>
											<th data-field="price" width="15%">Jumlah</th>
										</tr>

									</thead>
									<tbody id="tbody_cetak">
										<tr>
												<td colspan="7"><center>Tidak Ada Data Penjualan</center></td>
										</tr>
									</tbody>
							</table>
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->

		<!-- Modal -->
		<div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog modal-lg">
		      	<div class="modal-content">
		        	<div class="modal-header">
		          		<button type="button" class="close" data-dismiss="modal">&times;</button>
		          		<h4 class="modal-title">No Nota : <span id="judul"></span></h4>
		        	</div>
			        <div class="modal-body">
			        <table class="table table-bordered table-hover">
						    <thead>
							    <tr>
							    	<th data-align="right" width="5%">No.</th>
							    	<th data-field="name" width="30%">Nama Barang</th>
							        <th data-field="satuan" data-align="right%" width="15%">Satuan</th>
							        <th data-field="qty" width="5%">Qty</th>
							        <th data-field="price" width="15%">Harga</th>
							        <th data-field="price" width="20%">Jumlah</th>
							    </tr>

						    </thead>
						    <tbody id="tbody_detail">
						    	<tr>
							        <td colspan="7"><center>Tidak Ada Data Barang</center></td>
							    </tr>
						    </tbody>
						</table>
						<div class="col-md-9"></div>
						<div class="col-md-3 no-padding-right"><input type="text" class="form-control" id="totalpenjualan" disabled="" /></div>
						<div style="clear:both;">&nbsp;</div>
			        </div>
			        <div class="modal-footer">
			          	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        </div>
		      	</div>
		    </div>
		</div>

	</div><!--/.main-->

	<script src="<?=base_url()?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap-datepicker.js"></script>
	<script>
		$(document).ready(function(){
			$("#tanggal_dari").val(getDateNow());
			$("#tanggal_sampai").val(getDateNow());

			$('#searchPenjualan').submit(function(e){
				e.preventDefault();
				var dari = $("#tanggal_dari").val();
				var dariformat = formatedDate(dari);
				var sampai = $("#tanggal_sampai").val();
				var sampaiformat = formatedDate(sampai);
				var data = {};
				data['dari'] = dariformat+" 00:00:00";
				data['sampai'] = sampaiformat+" 23:59:59";

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>NotaHutang/selectNotaPenjualan",
			        data: data,
			        dataType: 'json',
			        success: function(data){
			        	$('#tbody_penjualan').empty();
			        	console.log(data);
			        	if(data.length>0)
			            for(var i= 0; i<data.length;i++){
			            	var nama = "";
										var hutang = Number(data[i]['total_bayar'])-Number(data[i]['pembayaran']);
			            	if(data[i]['nama']==null)
			            		nama = "-"
			            	else
			            		nama = data[i]['nama']

			            	$('#tbody_penjualan').append(
		            			'<tr>'
							        +'<td>'+(i+1)+'</td>'
							        +'<td>'+data[i]['no_nota']+'</td>'
							        +'<td>'+data[i]['waktu']+'</td>'
							        +'<td>'+nama+'</td>'
											+'<td>'+(data[i]['total_bayar'])+'</td>'
							        +'<td>'+hutang+'</td>'
							        +'<td>'
							        	+'<button class="btn btn-warning  btn-glyp viewData" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-search"></span></button>'
							        +'</td>'
							    +'</tr>'
		            		);
			            }
			            else
			            	$('#tbody_penjualan').append('<tr><td colspan="6"><center>Tidak Tersedia Data</center></td></tr>');
			        },error:function(data){
								console.log(data);
							}
			    });

					$.ajax({
				        type: "POST",
				        url: "<?=base_url()?>NotaHutang/selectCetakPenjualan",
				        data: data,
				        dataType: 'json',
				        success: function(data){
				        	$('#tbody_cetak').empty();
				        	console.log(data);
				        	if(data.length>0){
										var total = 0;
				            for(var i= 0; i<data.length;i++){
												var jumlah = Number(data[i]['qty'])*Number(data[i]['harga_jual']);
												total += jumlah;

					            	var nama = "";
					            	if(data[i]['nama']==null)
					            		nama = "-"
					            	else
					            		nama = data[i]['nama']

					            	$('#tbody_cetak').append(
				            			'<tr>'
									        +'<td>'+data[i]['no_nota']+'</td>'
									        +'<td>'+data[i]['waktu']+'</td>'
									        +'<td>'+nama+'</td>'
													+'<td>'+data[i]['nama_barang']+'</td>'
													+'<td>'+data[i]['qty']+'</td>'
									        +'<td>'+data[i]['harga_jual']+'</td>'
													+'<td>'+jumlah+'</td>'
									    +'</tr>'
				            		);
					            }
										}
				            else
				            	$('#tbody_cetak').append('<tr><td colspan="7"><center>Tidak Tersedia Data</center></td></tr>');
				        },error:function(data){
									console.log(data);
								}
				    });
			});

			$(document).on('click','.viewData', function(){
				var item = {};
				item['no_nota'] = $(this).closest('tr').find('td').eq(1).text();
				$('#judul').text(item['no_nota']);
				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>NotaPenjualan/selectDataNotaId",
			        data: item,
			        dataType: 'json',
			        success: function(data){
			            if(data.length>0){
			            	$('#tbody_detail').empty();
			            	var total = 0;
			            	for(var i = 0; i<data.length; i++){
			            		var jumlah = Number(data[i]['qty'])*Number(data[i]['harga_jual']);
			            		total+=jumlah;
			            		$('#tbody_detail').append(
			            			'<tr>'
								        +'<td>'+(i+1)+'</td>'
								        +'<td>'+data[i]['nama_barang']+'</td>'
								        +'<td>'+data[i]['satuan']+'</td>'
								        +'<td>'+data[i]['qty']+'</td>'
								        +'<td>'+data[i]['harga_jual']+'</td>'
								        +'<td>'+jumlah+'</td>'
								    +'</tr>'
			            		);
			            	}
			            	$('#totalpenjualan').val(total);
			            }else{
			            	$('#tbody_barang').empty();
			            	$('#tbody_barang').append('<tr><td colspan="7"><center>Tidak Ada Data Barang</center></td></tr>');
			            }
			        },error:function(data){
			        	console.log(data);
			        }
			    });
			})

			$('#printData').click(function(){
				var dari = $("#tanggal_dari").val();
				var dariformat = formatedDate(dari);
				var sampai = $("#tanggal_sampai").val();
				var sampaiformat = formatedDate(sampai);

					$('#tanggalcetak').text(dariformat+" sampai "+sampaiformat);
					$('#tanggalcetak').show();
					$('#cetakTabel').show();
					$('#sidebar-collapse').hide();
					$('#navRemove').hide();
					$('#tabelAsli').hide();
					$('#searchPenjualan').hide();
					window.print();
					$('#sidebar-collapse').show();
					$('#navRemove').show();
					$('#cetakTabel').hide();
					$('#tabelAsli').show();
					$('#tanggalcetak').hide();
					$('#searchPenjualan').show();
			})
		})

		function formatedDate(tanggal){
			tanggal = tanggal.split("/");
			var formated = tanggal[2]+"-"+tanggal[0]+"-"+tanggal[1];
			return formated;
		}

		function checkTime(i) {
	        return (i < 10) ? "0" + i : i;
	    }

		function getDateNow(){
			var today = new Date(),
            h = checkTime(today.getHours()),
            m = checkTime(today.getMinutes()),
            s = checkTime(today.getSeconds());
            d = checkTime(today.getDate());
            mt = checkTime(today.getMonth()+1);
            y = today.getFullYear();
            return mt+"/"+d+"/"+y;
		}
	</script>
</body>

</html>
