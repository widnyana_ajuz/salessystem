<?php
	if (!isset($this->session->userdata['logged_in'])) {
		header("location: ".base_url());
	}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Toko Online</title>

<link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/datepicker3.css" rel="stylesheet">
<link href="<?=base_url()?>assets/css/styles.css" rel="stylesheet">

<style type="text/css">
	@media print{
		body * {visibility: hidden;}
		.panel-heading {visibility: visible; width: 100%;top: -50px; left:0px}
		#cetakTabel * {visibility: visible; font-size: 12px;}
		#cetakTabel {position: absolute; width: 100%;top: -50px; left:0px;}
		#cetakTabel tbody tr td {border:solid 1px !important;}
		#tanggalcetak {visibility: visible;}
		#cetakTabel tbody tr {border:solid 1px !important; padding:0px !important;}
		#cetakTabel tbody tr td {border:solid 1px !important; padding:0px !important;}
		#cetakTabel thead tr td {border:solid 1px !important; padding:0px !important;}
		#cetakTabcetakTabelelRecipt thead tr th {border:solid 1px !important; height:10px; padding:0px !important;}
		.main{margin-left:0px !important; margin-top: -50px; width: 100%;}

		#heading-print {font-size: 8px;}
		#cetakTabelRecipt * {visibility: visible; font-size: 8px;}
		#cetakTabelRecipt {position: fixed; width: 150px !important; top: 0; left:0;}
		#cetakTabelRecipt tbody tr {border:solid 1px !important; padding:0px !important;}
		#cetakTabelRecipt tbody tr td {border:solid 1px !important; padding:0px !important;}
		#cetakTabelRecipt thead tr td {border:solid 1px !important; padding:0px !important;}
		#cetakTabelRecipt thead tr th {border:solid 1px !important; height:10px; padding:0px !important;}

		#cetakTabelMaster * {visibility: visible; font-size: 12px;}
		#cetakTabelMaster {position: absolute; width: 100%;top: -700px; left:0px;}
		#cetakTabelMaster tbody tr td {border:solid 1px !important;}
		#cetakTabelMaster tbody tr {border:solid 1px !important; padding:0px !important;}
		#cetakTabelMaster tbody tr td {border:solid 1px !important; padding:0px !important;}
		#cetakTabelMaster thead tr td {border:solid 1px !important; padding:0px !important;}
	}


</style>

<script src="<?=base_url()?>assets/js/lumino.glyphs.js"></script>

</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" id="navRemove" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Sistem</span>Toko</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> <?php echo $this->session->userdata['logged_in']['username']; ?> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?=base_url()?>home/logout"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>

		</div><!-- /.container-fluid -->
	</nav>

	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">

			</div>
		</form>
		<ul class="nav menu">
			<li><a href="<?=base_url()?>Penjualan" class="font-bigger"><svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg> Penjualan</a></li>
			<!-- <li><a href="icons.html" class="font-bigger"><svg class="glyph stroked cliboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg> Nota Bon</a></li> -->
			<li><a href="<?=base_url()?>NotaPenjualan" class="font-bigger"><svg class="glyph stroked notepad"><use xlink:href="#stroked-notepad"></use></svg> Laporan Penjualan</a></li>
			<li><a href="<?=base_url()?>NotaHutang" class="font-bigger"><svg class="glyph stroked notepad"><use xlink:href="#stroked-notepad"></use></svg> Laporan Hutang</a></li>
			<li role="presentation" class="divider"></li>
			<li><a href="<?=base_url()?>Masterbarang" class="font-bigger"><svg class="glyph stroked basket"><use xlink:href="#stroked-basket"></use></svg> Master Barang</a></li>
			<li><a href="<?=base_url()?>Masterpelanggan" class="font-bigger"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Master Pelanggan</a></li>
		</ul>

	</div><!--/.sidebar-->
