	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Data Barang</div>
					<div class="panel-body">
						<div class="col-md-12">
							<form role="form" id="submitInsert">
								<div class="col-md-12 no-padding">
									<div class="form-group col-md-3 ">
										<label>Nama Barang</label>
										<input class="form-control" id="nama_barang" placeholder="Nama Barang" required>
									</div>
									<div class="form-group col-md-2 no-padding">
										<label>Satuan 1</label>
										<input class="form-control" id="satuan" placeholder="Satuan" required>
									</div>
									<div class="form-group col-md-2 no-padding-right">
										<label>Jual 1</label>
										<input type="number" id="harga_jual" class="form-control" placeholder="Harga Jual" required>
									</div>
									<div class="form-group col-md-2 no-padding-right">
										<label>Satuan 2</label>
										<input class="form-control" id="satuan2" placeholder="Satuan" required>
									</div>
									<div class="form-group col-md-2 no-padding-right">
										<label>Jual 2</label>
										<input type="number" id="harga_jual2" class="form-control" placeholder="Harga Jual" required>
									</div>
									<div class="form-group col-md-1">
										<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
										<button type="submit" class="btn btn-info button-full"><span class="glyphicon glyphicon-plus"></span></button>
									</div>
								</div>
							</form>
							<hr>
							<div class="col-md-12 border-top">
								<div class="form-group col-md-2 no-padding-left">
									<button class="btn btn-info button-full" id="printData">Print Data</button>
								</div>
								<div class="form-group col-md-7">
								</div>
								<div class="form-group col-md-3 no-padding">
									<form id="submitSearch"><input type="text" id="inputSearch" class="form-control" placeholder="Search Barang"></form>
								</div>
								<div class="col-md-12 no-padding">
								<form id="formSubmitEdit">
								<table class="table table-bordered table-hover" id="cetakTabelMaster">
								    <thead>
									    <tr id="thead_barang">
									    	<th data-align="right" width="3%">No.</th>
									    	<th data-field="name" width="25%">Nama Barang</th>
									        <th data-field="satuan" data-align="right%" width="15%">Satuan 1</th>
									        <th data-field="price" width="15%">Jual 1</th>
													<th data-field="satuan" data-align="right%" width="15%">Satuan 2</th>
									        <th data-field="price" width="15%">Jual 2</th>
									        <th data-field="price" width="10%">Action</th>
									    </tr>
								    </thead>
								    <tbody id="tbody_barang">
								    	<?php
								    		if(!empty($databarang)){
								    			$nomor = 1;
									    		foreach ($databarang as $data) {
									    			echo '
									    				<tr>
													        <td>'.$nomor.'</td>
													        <td>'.$data['nama_barang'].'</td>
													        <td>'.$data['satuan'].'</td>
													        <td>'.$data['harga_jual'].'</td>
																	<td>'.$data['satuan2'].'</td>
													        <td>'.$data['harga_jual2'].'</td>
													        <td>
													        	<input type="hidden" class="id_barang" value="'.$data['id_barang'].'">
													        	<span class="btn btn-warning  btn-glyp editBarang glyphicon glyphicon-edit"></span>
													        	<span class="btn btn-danger  btn-glyp deleteBarang glyphicon glyphicon-trash"></span>
																		<button class="btn-glyp btn btn-success submitEdit" style="display:none;""><span class="glyphicon glyphicon-ok"></span></button>
																		<span class="glyphicon glyphicon-remove btn-glyp btn btn-info cancelEdit" style="display:none;"></span>
													        </td>
													    </tr>
									    			';
									    			$nomor++;
									    		}
									    	}else{
									    		echo '
									    			<tr><td colspan="6"><center>Tidak Tersedia Data</center></td></tr>
									    		';
									    	}
								    	?>
								    </tbody>
								</table>
							</form>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->

	</div><!--/.main-->

	<script src="<?=base_url()?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap-datepicker.js"></script>
	<script>
		$(document).ready(function(){
			// $('#submitEdit').hide();
			var id_barang;
			var nama_barang;
			var satuan;
			var harga_beli;
			var harga_jual;
			var satuan2;
			var harga_jual2;

			$(document).on('click','.editBarang', function(){

				id_barang = $(this).parent().find('.id_barang').val();
				nama_barang = $(this).closest('tr').find('td').eq(1).text();
				satuan = $(this).closest('tr').find('td').eq(2).text();
				harga_jual = $(this).closest('tr').find('td').eq(3).text();
				satuan2 = $(this).closest('tr').find('td').eq(4).text();
				harga_jual2 = $(this).closest('tr').find('td').eq(5).text();

				//edit nama barang
				$(this).closest('tr').find('td').eq(1).empty();
				$(this).closest('tr').find('td').eq(1).append('<input class="form-control" id="editnama_barang" placeholder="Nama Barang" required>');
				$(this).closest('tr').find('td').eq(1).find('#editnama_barang').val(nama_barang);

				$(this).closest('tr').find('td').eq(2).empty();
				$(this).closest('tr').find('td').eq(2).append('<input class="form-control" id="editsatuan" placeholder="Satuan" required>');
				$(this).closest('tr').find('td').eq(2).find('#editsatuan').val(satuan);

				$(this).closest('tr').find('td').eq(3).empty();
				$(this).closest('tr').find('td').eq(3).append('<input type="number" id="editharga_jual" class="form-control" placeholder="Harga Jual" required>');
				$(this).closest('tr').find('td').eq(3).find('#editharga_jual').val(harga_jual);

				$(this).closest('tr').find('td').eq(4).empty();
				$(this).closest('tr').find('td').eq(4).append('<input class="form-control" id="editsatuan2" placeholder="Satuan" required>');
				$(this).closest('tr').find('td').eq(4).find('#editsatuan2').val(satuan2);

				$(this).closest('tr').find('td').eq(5).empty();
				$(this).closest('tr').find('td').eq(5).append('<input type="number" id="editharga_jual2" class="form-control" placeholder="Harga Jual" required>');
				$(this).closest('tr').find('td').eq(5).find('#editharga_jual2').val(harga_jual2);

				$(this).closest('tr').find('td').eq(6).find('.editBarang').hide();
				$(this).closest('tr').find('td').eq(6).find('.deleteBarang').hide();
				$(this).closest('tr').find('td').eq(6).find('.submitEdit').show();
				$(this).closest('tr').find('td').eq(6).find('.cancelEdit').show();

				$('#editnama_barang').focus();
			})

			$(document).on('click','.cancelEdit', function(){
				$(this).closest('tr').find('td').eq(6).find('.editBarang').show();
				$(this).closest('tr').find('td').eq(6).find('.deleteBarang').show();
				$(this).closest('tr').find('td').eq(6).find('.submitEdit').hide();
				$(this).closest('tr').find('td').eq(6).find('.cancelEdit').hide();

				$(this).closest('tr').find('td').eq(1).text(nama_barang);
				$(this).closest('tr').find('td').eq(2).text(satuan);
				$(this).closest('tr').find('td').eq(3).text(harga_jual);
				$(this).closest('tr').find('td').eq(4).text(satuan2);
				$(this).closest('tr').find('td').eq(5).text(harga_jual2);
			})

			$('#formSubmitEdit').submit(function(e){
				e.preventDefault();
				var data = {};
				data['id'] = $(this).parent().find('.id_barang').val();
				data['nama'] = $('#editnama_barang').val();
				data['satuan'] = $('#editsatuan').val();
				data['hj'] = $('#editharga_jual').val();
				data['satuan2'] = $('#editsatuan2').val();
				data['hj2'] = $('#editharga_jual2').val();

				var con = confirm("Edit Data Barang?");

				if(con==false)
					return false;

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>masterbarang/editDataBarang",
			        data: data,
			        dataType: 'json',
			        success: function(data){
			            alert(data['message']);
									$(this).closest('tr').find('td').eq(6).find('.editBarang').hide();
									$(this).closest('tr').find('td').eq(6).find('.deleteBarang').hide();
									$(this).closest('tr').find('td').eq(6).find('.submitEdit').show();
									$(this).closest('tr').find('td').eq(6).find('.cancelEdit').show();
			            updateTabelBarang();
			        },error: function(data){
			        	console.log(data);
			        }
			    });
			})

			$(document).on('click','.deleteBarang', function(){
				var data = {};
				data['id_barang'] = $(this).parent().find('.id_barang').val();

				var con = confirm("Hapus Data?");

				if(con==false)
					return false;

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>masterbarang/deleteDataBarang",
			        data: data,
			        dataType: 'json',
			        success: function(data){
			            alert(data['message']);
			            updateTabelBarang();
			        },error: function(data){
			        	console.log(data);
			        }
			    });
			})

			$('#submitInsert').submit(function(e){
				e.preventDefault();
				var data = {};
				data['nama_barang'] = $('#nama_barang').val();
				data['satuan'] = $('#satuan').val();
				data['harga_jual'] = $('#harga_jual').val();
				data['satuan2'] = $('#satuan2').val();
				data['harga_jual2'] = $('#harga_jual2').val();

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>masterbarang/addDataBarang",
			        data: data,
			        dataType: 'json',
			        success: function(data){
			            alert(data['message']);
			            $('#nama_barang').val('');
			            $('#satuan').val('');
			            $('#harga_jual').val('');
									$('#satuan2').val('');
			            $('#harga_jual2').val('');
									$('#nama_barang').focus();
			            updateTabelBarang();
			        },error: function(data){
			        	console.log(data);
			        }
			    });
			})

			$('#inputSearch').keyup(function(e){
				var data = {};
				data['keyword'] = $('#inputSearch').val();
				e.preventDefault();

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>masterbarang/searchDataBarang",
			        data: data,
			        dataType: 'json',
			        success: function(data){
			            if(data.length>0){
			            	$('#tbody_barang').empty();
			            	for(var i = 0; i<data.length; i++){
			            		$('#tbody_barang').append(
			            			'<tr>'
								        +'<td>'+(i+1)+'</td>'
								        +'<td>'+data[i]['nama_barang']+'</td>'
								        +'<td>'+data[i]['satuan']+'</td>'
								        +'<td>'+data[i]['harga_jual']+'</td>'
												+'<td>'+data[i]['satuan2']+'</td>'
								        +'<td>'+data[i]['harga_jual2']+'</td>'
								        +'<td>'
								        	+'<input type="hidden" class="id_barang" value="'+data[i]['id_barang']+'">'
													+'<span class="btn btn-warning  btn-glyp editBarang glyphicon glyphicon-edit"></span>'
													+'<span class="btn btn-danger  btn-glyp deleteBarang glyphicon glyphicon-trash"></span>'
													+'<button class="btn-glyp btn btn-success submitEdit" style="display:none;""><span class="glyphicon glyphicon-ok"></span></button>'
													+'<span class="glyphicon glyphicon-remove btn-glyp btn btn-info cancelEdit" style="display:none;"></span>'
								        +'</td>'
								    +'</tr>'
			            		);
			            	}
			            }else{
			            	$('#tbody_barang').empty();
			            	$('#tbody_barang').append('<tr><td colspan="6"><center>Data tidak ditemukan</center></td></tr>');
			            }
			        }
			    });
			})

			$('#printData').click(function(){
					$('#cetakTabelMaster th:last-child, #cetakTabelMaster td:last-child').remove();
					window.print();
					$('#thead_barang').append('<th data-field="price" width="10%">Action</th>');
					updateTabelBarang();
			})
		})

		function updateTabelBarang(){
			var data = {};
			data['keyword'] = "";

			$.ajax({
		        type: "POST",
		        url: "<?=base_url()?>masterbarang/searchDataBarang",
		        data: data,
		        dataType: 'json',
		        success: function(data){
		            if(data.length>0){
		            	$('#tbody_barang').empty();
		            	for(var i = 0; i<data.length; i++){
		            		$('#tbody_barang').append(
		            			'<tr>'
							        +'<td>'+(i+1)+'</td>'
							        +'<td>'+data[i]['nama_barang']+'</td>'
							        +'<td>'+data[i]['satuan']+'</td>'
							        +'<td>'+data[i]['harga_jual']+'</td>'
											+'<td>'+data[i]['satuan2']+'</td>'
							        +'<td>'+data[i]['harga_jual2']+'</td>'
							        +'<td>'
							        	+'<input type="hidden" class="id_barang" value="'+data[i]['id_barang']+'">'
							        	+'<span class="btn btn-warning  btn-glyp editBarang glyphicon glyphicon-edit"></span>'
												+'<span class="btn btn-danger  btn-glyp deleteBarang glyphicon glyphicon-trash"></span>'
												+'<button class="btn-glyp btn btn-success submitEdit" style="display:none;""><span class="glyphicon glyphicon-ok"></span></button>'
												+'<span class="glyphicon glyphicon-remove btn-glyp btn btn-info cancelEdit" style="display:none;"></span>'
							        +'</td>'
							    +'</tr>'
		            		);
		            	}
		            }else{
		            	$('#tbody_barang').empty();
		            	$('#tbody_barang').append('<tr><td colspan="6"><center>Data tidak ditemukan</center></td></tr>');
		            }
		        }
		    });
		}

	</script>
</body>
</html>
