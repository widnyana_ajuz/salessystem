<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Smart Analizer Bot for Binary with Javascript">
	<meta name="keywords" content="Smart Analizer Bot, Bot, Analizer, Binary.com, Trading, Sabot" />
	<meta name="author" content="">
	<title>Smart Analizer Bot</title>
	<link href="<?=base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/prettyPhoto.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/animate.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/main.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/responsive.css" rel="stylesheet">
	<!--[if lt IE 9]> <script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script> <![endif]-->
	<link rel="shortcut icon" href="images/ico/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=base_url()?>assets/images/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=base_url()?>assets/images/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=base_url()?>assets/images/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="<?=base_url()?>assets/images/ico/apple-touch-icon-57-precomposed.png">
	<style type="text/css">
	</style>
</head><!--/head-->
<body>
	<section id="contact" style="min-height:670px;">
		<div class="container">
			<div class="row text-center clearfix">
				<div class="col-sm-8 col-sm-offset-2">
					<div class="contact-heading">
						<h1 class="title-two">SabotJS Login</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="contact-details">
				<div class="pattern"></div>
				<div class="row text-center clearfix">
					<div class="col-sm-3"> </div>
					<div class="col-sm-6">
						<div id="contact-form-section" style="height:auto;">
							<div class="status alert alert-success" style="display: none"></div>
							<form id="contact-form" class="contact" name="contact-form" method="post" action="<?=base_url()?>sabotjs/submitLogin">
									<div class="col-sm-3 form-group">
										<label class="label-login">Email :</label>
									</div>
									<div class="col-sm-9 form-group">
										<input type="email" name="email" class="form-control" required="required" placeholder="Your Email">
									</div>
									<div class="col-sm-3 form-group">
										<label class="label-login">Password :</label>
									</div>
									<div class="col-sm-9 form-group">
										<input type="password" name="email" class="form-control" required="required" placeholder="Your Password">
									</div>
									<div class="col-sm-9 form-group">

									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-primary">Login</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> <!--/#contact-->

</body>
</html>
