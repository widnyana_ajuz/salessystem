	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Data Pelanggan</div>
					<div class="panel-body">
						<div class="col-md-12">
							<form role="form" id="submitInsert">
								<div class="col-md-12 no-padding">
									<div class="form-group col-md-3 ">
										<label>Nama</label>
										<input class="form-control" id="namapelanggan" placeholder="Nama">
									</div>
									<div class="form-group col-md-1 no-padding">
										<label>Status</label>
										<input class="form-control" id="statuspelanggan" placeholder="Status">
									</div>
									<div class="form-group col-md-2 no-padding-right">
										<label>No Telpon</label>
										<input type="text" id="telppelanggan" class="form-control" placeholder="No Telpon" >
									</div>
									<div class="form-group col-md-3 no-padding-right">
										<label>Alamat</label>
										<input type="text" id="alamatpelanggan"  class="form-control" placeholder="Alamat" >
									</div>
									<div class="form-group col-md-2 no-padding-right">
										<label>Batas Transaksi</label>
										<input type="number" id="batastransaksi"  class="form-control" placeholder="Batas Transaksi" >
									</div>

									<div class="form-group col-md-1">
										<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
										<button class="btn btn-info button-full"><span class="glyphicon glyphicon-plus"></span></button>
									</div>
								</div>
							</form>
							<form role="form" id="submitEdit">
								<div class="col-md-12 no-padding">
									<div class="form-group col-md-3 ">
										<label>Nama</label>
										<!-- <input class="form-control" id="editnama" placeholder="Nama"> -->
									</div>
									<div class="form-group col-md-1 no-padding">
										<label>Status</label>
										<!-- <input class="form-control" id="editstatus" placeholder="Status"> -->
									</div>
									<div class="form-group col-md-2 no-padding-right">
										<label>No Telpon</label>
										<!-- <input type="text" class="form-control" id="edittelp" placeholder="No Telpon" > -->
									</div>
									<div class="form-group col-md-4 no-padding-right">
										<label>Alamat</label>
										<!-- <input type="text"  class="form-control" id="editalamat" placeholder="Alamat" > -->
									</div>
									<div class="form-group col-md-2">
										<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
										<br>
										<input type="hidden" id="editid_pelanggan">
										<button type="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok"></span></button>
										<span class="glyphicon glyphicon-remove btn btn-danger" id="cancelEdit"></span>
									</div>
								</div>
							</form>

							<hr>
							<div class="col-md-12 border-top">
								<div class="form-group col-md-2 no-padding-left">
									<button class="btn btn-info button-full">Print Data</button>
								</div>
								<div class="form-group col-md-7">
								</div>
								<div class="form-group col-md-3 no-padding">
									<input type="text" class="form-control" id="inputSearch" placeholder="Search Pelanggan">
								</div>
								<table class="table table-bordered table-hover">
								    <thead>
									    <tr>
									    	<th data-align="right" width="3%">No.</th>
									    	<th data-field="name" width="25%">Nama</th>
									        <th data-field="satuan" data-align="right%" width="5%">Status</th>
									        <th data-field="qty" width="10%">No Telpon</th>
									        <th data-field="price" width="15%">Alamat</th>
									        <th data-field="price" width="10%">Batas Transaksi</th>
									        <th data-field="price" width="10%">Action</th>
									    </tr>

								    </thead>
								    <tbody id="tbody_pelanggan">
								    	<?php
								    		if(!empty($datapelanggan)){
								    			$nomor = 1;
									    		foreach ($datapelanggan as $data) {
									    			echo '
									    				<tr>
													        <td>'.$nomor.'</td>
													        <td>'.$data['nama'].'</td>
													        <td>'.$data['status'].'</td>
													        <td>'.$data['telepon'].'</td>
													        <td>'.$data['alamat'].'</td>
													        <td>'.$data['batas_transaksi'].'</td>
													        <td>
													        	<input type="hidden" class="id_pelanggan" value="'.$data['id_pelanggan'].'">
													        	<button class="btn btn-warning  btn-glyp editPelanggan"><span class="glyphicon glyphicon-edit"></span></button>
													        	<button class="btn btn-danger  btn-glyp deletePelanggan"><span class="glyphicon glyphicon-trash"></span></button>
																		<span class="glyphicon glyphicon-ok btn-glyp btn btn-success submitEdit" style="display:none;"></span>
																		<span class="glyphicon glyphicon-remove btn-glyp btn btn-info cancelEdit" style="display:none;"></span>
													        </td>
													    </tr>
									    			';
									    			$nomor++;
									    		}
									    	}else{
									    		echo '
									    			<tr><td colspan="6"><center>Tidak Tersedia Data</center></td></tr>
									    		';
									    	}
								    	?>
								    </tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->

	</div><!--/.main-->

	<script src="<?=base_url()?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap-datepicker.js"></script>
	<script>
		$(document).ready(function(){
			var id_pelanggan;
			var nama;
			var status;
			var telepon;
			var alamat;
			var batasan;

			$(document).on('click','.editPelanggan', function(){

				id_pelanggan = $(this).parent().find('.id_pelanggan').val();
				nama = $(this).closest('tr').find('td').eq(1).text();
				status = $(this).closest('tr').find('td').eq(2).text();
				telepon = $(this).closest('tr').find('td').eq(3).text();
				alamat = $(this).closest('tr').find('td').eq(4).text();
				alamat = $(this).closest('tr').find('td').eq(5).text();

				$(this).closest('tr').find('td').eq(1).empty();
				$(this).closest('tr').find('td').eq(1).append('<input class="form-control" id="editnama" placeholder="Nama">');
				$(this).closest('tr').find('td').eq(1).find('#editnama').val(nama);

				$(this).closest('tr').find('td').eq(2).empty();
				$(this).closest('tr').find('td').eq(2).append('<input class="form-control" id="editstatus" placeholder="Status">');
				$(this).closest('tr').find('td').eq(2).find('#editstatus').val(status);

				$(this).closest('tr').find('td').eq(3).empty();
				$(this).closest('tr').find('td').eq(3).append('<input type="text" class="form-control" id="edittelp" placeholder="No Telpon" >');
				$(this).closest('tr').find('td').eq(3).find('#edittelp').val(telepon);

				$(this).closest('tr').find('td').eq(4).empty();
				$(this).closest('tr').find('td').eq(4).append('<input type="text"  class="form-control" id="editalamat" placeholder="Alamat" >');
				$(this).closest('tr').find('td').eq(4).find('#editalamat').val(alamat);

				$(this).closest('tr').find('td').eq(5).empty();
				$(this).closest('tr').find('td').eq(5).append('<input type="text"  class="form-control" id="editbatasan" placeholder="Alamat" >');
				$(this).closest('tr').find('td').eq(5).find('#editbatasan').val(batasan);

				$(this).closest('tr').find('td').eq(6).find('.editPelanggan').hide();
				$(this).closest('tr').find('td').eq(6).find('.deletePelanggan').hide();
				$(this).closest('tr').find('td').eq(6).find('.submitEdit').show();
				$(this).closest('tr').find('td').eq(6).find('.cancelEdit').show();

				$('#editnama').focus();
			})

			$(document).on('click','.cancelEdit', function(){
				$(this).closest('tr').find('td').eq(6).find('.editPelanggan').show();
				$(this).closest('tr').find('td').eq(6).find('.deletePelanggan').show();
				$(this).closest('tr').find('td').eq(6).find('.submitEdit').hide();
				$(this).closest('tr').find('td').eq(6).find('.cancelEdit').hide();

				$(this).closest('tr').find('td').eq(1).text(nama);
				$(this).closest('tr').find('td').eq(2).text(status);
				$(this).closest('tr').find('td').eq(3).text(telepon);
				$(this).closest('tr').find('td').eq(4).text(alamat);
				$(this).closest('tr').find('td').eq(5).text(batasan);
			})

			$(document).on('click','.submitEdit', function(){
				var data = {};
				data['id'] = $(this).parent().find('.id_pelanggan').val();
				data['nama'] = $('#editnama').val();
				data['status'] = $('#editstatus').val();
				data['telepon'] = $('#edittelp').val();
				data['alamat'] = $('#editalamat').val();
				data['batas_transaksi'] = $('#editbatasan').val();

				var con = confirm("Edit Data Pelanggan?");

				if(con==false)
					return false;

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>masterpelanggan/editDataPelanggan",
			        data: data,
			        dataType: 'json',
			        success: function(data){
			            alert(data['message']);
			            $('#submitEdit').hide();
						$('#submitInsert').show();
			            updateTabelPelanggan();
			        },error: function(data){
			        	console.log(data);
			        }
			    });
			})

			$(document).on('click','.deletePelanggan', function(){
				var data = {};
				data['id_pelanggan'] = $(this).parent().find('.id_pelanggan').val();

				alert(data['id_pelanggan']);

				var con = confirm("Hapus Data?");

				if(con==false)
					return false;

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>masterpelanggan/deleteDataPelanggan",
			        data: data,
			        dataType: 'json',
			        success: function(data){
			            alert(data['message']);
			            updateTabelpelanggan();
			        },error: function(data){
			        	console.log(data);
			        }
			    });
			})

			$('#submitInsert').submit(function(e){
				e.preventDefault();
				var data = {};
				data['nama'] = $('#namapelanggan').val();
				data['status'] = $('#statuspelanggan').val();
				data['telepon'] = $('#telppelanggan').val();
				data['alamat'] = $('#alamatpelanggan').val();
				data['batas_transaksi'] = $('#batastransaksi').val();

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>masterpelanggan/addDataPelanggan",
			        data: data,
			        dataType: 'json',
			        success: function(data){
			            alert(data['message']);
			            $('#namapelanggan').val('');
			            $('#statuspelanggan').val('');
			            $('#telppelanggan').val('');
			            $('#alamatpelanggan').val('');
			            updateTabelPelanggan();
			        },error: function(data){
			        	console.log(data);
			        }
			    });
			})

			$('#inputSearch').keyup(function(e){
				var data = {};
				data['keyword'] = $('#inputSearch').val();
				e.preventDefault();

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>masterpelanggan/searchDataPelanggan",
			        data: data,
			        dataType: 'json',
			        success: function(data){
			            if(data.length>0){
			            	$('#tbody_pelanggan').empty();
			            	for(var i = 0; i<data.length; i++){
			            		$('#tbody_pelanggan').append(
			            			'<tr>'
								        +'<td>'+(i+1)+'</td>'
								        +'<td>'+data[i]['nama']+'</td>'
								        +'<td>'+data[i]['status']+'</td>'
								        +'<td>'+data[i]['telepon']+'</td>'
								        +'<td>'+data[i]['alamat']+'</td>'
								        +'<td>'
								        	+'<input type="hidden" class="id_pelanggan" value="'+data[i]['id_pelanggan']+'">'
								        	+'<button class="btn btn-warning  btn-glyp editPelanggan"><span class="glyphicon glyphicon-edit"></span></button>'
								        	+'<button class="btn btn-danger  btn-glyp deletePelanggan"><span class="glyphicon glyphicon-trash"></span></button>'
													+'<span class="glyphicon glyphicon-ok btn-glyp btn btn-success submitEdit" style="display:none;"></span>'
													+'<span class="glyphicon glyphicon-remove btn-glyp btn btn-info cancelEdit"  style="display:none;"></span>'
								        +'</td>'
								    +'</tr>'
			            		);
			            	}
			            }else{
			            	$('#tbody_pelanggan').empty();
			            	$('#tbody_pelanggan').append('<tr><td colspan="6"><center>Data tidak ditemukan</center></td></tr>');
			            }
			        }
			    })
			})
		})

		function updateTabelPelanggan(){
			var data = {};
			data['keyword'] = "";

			$.ajax({
		        type: "POST",
		        url: "<?=base_url()?>masterpelanggan/searchDataPelanggan",
		        data: data,
		        dataType: 'json',
		        success: function(data){
		            if(data.length>0){
		            	$('#tbody_pelanggan').empty();
		            	for(var i = 0; i<data.length; i++){
		            		$('#tbody_pelanggan').append(
		            			'<tr>'
							        +'<td>'+(i+1)+'</td>'
							        +'<td>'+data[i]['nama']+'</td>'
							        +'<td>'+data[i]['status']+'</td>'
							        +'<td>'+data[i]['telepon']+'</td>'
							        +'<td>'+data[i]['alamat']+'</td>'
							        +'<td>'
							        	+'<input type="hidden" class="id_pelanggan" value="'+data[i]['id_pelanggan']+'">'
							        	+'<button class="btn btn-warning  btn-glyp editPelanggan"><span class="glyphicon glyphicon-edit"></span></button>'
							        	+'<button class="btn btn-danger  btn-glyp deletePelanggan"><span class="glyphicon glyphicon-trash"></span></button>'
												+'<span class="glyphicon glyphicon-ok btn-glyp btn btn-success submitEdit" style="display:none;"></span>'
												+'<span class="glyphicon glyphicon-remove btn-glyp btn btn-info cancelEdit" style="display:none;"></span>'
							        +'</td>'
							    +'</tr>'
		            		);
		            	}
		            }else{
		            	$('#tbody_pelanggan').empty();
		            	$('#tbody_pelanggan').append('<tr><td colspan="6"><center>Data tidak ditemukan</center></td></tr>');
		            }
		        }
		    });
		}
	</script>
</body>

</html>
