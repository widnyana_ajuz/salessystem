	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="col-md-9">Penjualan</div>
						<div class="col-md-3" style="text-align:right;" id="time"></div>
						<input type="hidden" id="tanggal">
					</div>
					<div class="panel-body">
						<div class="col-md-12" style="margin-top:-20px;">
							<div class="col-md-10 no-padding">
								<div class="form-group col-md-8 no-padding" style="padding:0px; !important">
									<table class="table table-no-border" style="border-color: #fff !important;">
									    <tbody>
										    <tr style="padding:5px;">
										    	<td width="15%">No Nota</td>
										    	<td width="35%"><input id="nonota" class="form-control" placeholder="Placeholder" value="<?php echo $no_nota; ?>" disabled="disabled"></td>
										    	<td width="15%"></td>
										    </tr>
										    <tr style="padding:5px;">
										    	<td width="15%">Pelanggan</td>
										    	<td width="20%"><input id="id_pelanggan" class="form-control typeaheadp input-order" placeholder="Nama Pelanggan"></td>
										    	<td width="15%"></td>
										    </tr>
									    </tbody>
									</table>
								</div>
							</div>
							<!-- <form role="form" id="submitBarang">
								<div class="col-md-10 no-padding">
									<div class="form-group col-md-4 no-padding-right">
										<label>Nama Barang</label>
										<input type="text" id="namabarang" class="form-control typeahead input-order" placeholder="Nama Barang" required>
									</div>
									<div class="form-group col-md-2 no-padding-right">
										<label>Qty.</label>
										<input type="number" id="qtybarang" class="form-control" placeholder="Qty" required>
									</div>
									<div class="form-group col-md-1" style="padding-left:10px;">
										<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
										<button class="btn btn-info button-full"><span class="glyphicon glyphicon-plus"></span></button>
									</div>
								</div>
							</form> -->
							<div class="col-md-2">
								<div id="notabaru" class="btn btn-warning" style="padding-top:10px;padding-bottom:10px;margin-top:35px;width:100%;">Nota Baru</div>
							</div>
							<div class="col-md-12" style="margin-top:-30px;">
								<table class="table table-bordered table-hover" id="cetakTabelRecipt">
								    <thead>
									    <tr>
												<th data-field="qty" width="10%">Qty</th>
												<th data-field="qty" width="14%">Satuan</th>
									    	<th data-field="name" width="35%">Nama Barang</th>
								        <th data-field="price" width="18%">Harga</th>
								        <th data-field="price" width="18%">Jumlah</th>
								        <th data-field="price" width="8%">Action</th>
									    </tr>
								    </thead>
								    <tbody id="tbody_barang">
								    	<tr>
									        <td colspan="7"><center>Tidak Ada Data Barang</center></td>
									    </tr>
								    </tbody>
										<tbody id="addBarangTabel">
											<form role="form" id="submitBarang">
												<tr>
													<td data-field="qty" width="10%"><input type="number" id="qtybarang" class="form-control" placeholder="Qty" required></td>
													<td data-field="qty" width="10%">
															<select id="satuanbarang" class="form-control" required="" name="satuan">
																<option value="">Satuan</option>
															</select>
													</td>
										    	<td data-field="name" width="35%"><input type="text" id="namabarang" class="form-control typeahead input-order" placeholder="Nama Barang" required></td>
									        <td data-field="price" width="20%"><input type="text" id="hargabarang" class="form-control typeahead input-order" placeholder="Harga" disabled></td>
									        <td data-field="price" width="20%"><input type="text" id="jumlahbarang" class="form-control typeahead input-order" placeholder="Jumlah" disabled></td>
									        <td data-field="price" width="10%"><input type="submit" style="display:none;"/><button class="btn btn-info button-full"><span class="glyphicon glyphicon-plus"></span></button></td>
										    </tr>
											</form>
										</tbody>
								</table>
							</div>
							<!-- <div class="form-group col-md-2">
										<label>Nomor Nota</label>
										<input class="form-control" placeholder="Placeholder" value="001" disabled="disabled">
									</div>
									<div class="form-group col-md-3 no-padding">
										<label>Nama Pelanggan</label>
										<input class="form-control typeaheadp input-order" placeholder="Nama Pelanggan">
									</div> -->
							<form id="submitNotaPenjualan">
							<div class="col-md-12" style="margin-top:-30px !important;">
								<div class="col-md-5 no-padding">
										<table class="table table-no-border" style="border-color: #fff !important;">
										    <tbody>
											    <tr>
											        <td data-field="price" width="10%">Hutang</td>
											        <td data-field="price" width="25%"><input type="number" id="hutangpelanggan" class="form-control" placeholder="Total" value="0" readonly></td>
											    </tr>
													<tr>
											        <td data-field="price" width="10%">Bayar Hutang</td>
											        <td data-field="price" width="25%"><select class="form-control" id="bayarhutang"><option value="0">Tidak</option><option value="1">Bayar</option></select></td>
											    </tr>
												</tbody>
											</table>
								</div>
								<div class="col-md-2 no-padding"></div>
								<div class="col-md-5 no-padding">
								<table class="table table-no-border" style="border-color: #fff !important;">
								    <tbody>
									    <tr>
									        <td data-field="price" width="10%">Total</td>
									        <td data-field="price" width="25%"><input type="number" id="totalpenjualan" class="form-control" placeholder="Total" value="0" readonly></td>
									    </tr>
									    <tr>
									        <td data-field="price" width="10%">Bayar</td>
									        <td data-field="price" width="25%"><input type="number" required id="pembayaran" class="form-control" placeholder="Bayar" required></td>

									    </tr>
									    <tr>
									        <td data-field="price" width="10%">Sisa</td>
									        <td data-field="price" width="25%"><input id="kembalian" type="text" class="form-control" placeholder="Sisa" disabled="disabled"></td>
									    </tr>
								    </tbody>
								</table>
								</div>
								<input type="hidden" value="true" id="canSubmit">
								<button type="submit" class="btn btn-success" id="printData">Simpan & Cetak Nota</button>
								<!-- <span type="submit" class="btn btn-danger" style="padding-top:9px;" id="printData" disabled>Bayar Hutang</span>
								<span type="submit" class="btn btn-warning" style="padding-top:9px;" id="printData" disabled>Masukan Hutang ke Total</span> -->
							</div>
							</form>
						</div>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->

	</div><!--/.main-->
	<link rel="stylesheet" href="<?=base_url()?>assets/css/jquery-ui.css">
	<script src="<?=base_url()?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/js/bootstrap-datepicker.js"></script>
	<script src="<?=base_url()?>assets/js/typeahead.min.js"></script>
	<style type="text/css">
		#scrollable-dropdown-menu .tt-dropdown-menu {
		  	max-height: 150px;
		  	overflow-y: auto;
		}
	</style>
	<script>
		$('#submitEdit').hide();
		var nama_barang;
		var qty;
		var confirmSubmit;
		var confirmBon;
		var batasTransaksi = 0;

		$('#namabarang').focus();

		$('#id_pelanggan').click(function(){
			$(this).focus();
		})

		$('#id_pelanggan').blur(function(){
			var item = {};
			var pelanggan = $('#id_pelanggan').val();
			var slice = pelanggan.split(" ");
			item['id'] = slice[slice.length-1];

			if(item['id']=="")
				$('#hutangpelanggan').val(0);
			else{
				$.ajax({
					type:"POST",
					url:"<?=base_url()?>penjualan/getHutangPelanggan",
					data:item,
					dataType: 'json',
					success:function(data){
						console.log(data);
						if(data==null)
							$('#hutangpelanggan').val(0);
						else{
							$('#hutangpelanggan').val(data['hutang']);
							batasTransaksi = data['batas_transaksi'];
							var isbayar = $("#bayarhutang").val();
							var total = $('#totalpenjualan').val();
							var hutangotal = Number(data['hutang'])+Number(total);
							
							if(batasTransaksi>total){
								$('#printData').prop("disabled", false);
								$('#canSubmit').val("true");
							}else{
								if(isbayar == 1){
									$('#totalpenjualan').val(hutangotal);
									$('#printData').prop("disabled", false);
									$('#canSubmit').val("true");
								}else{
									$('#printData').prop("disabled", true);
									$('#canSubmit').val("false");
								}
							}
						}
						$('#namabarang').focus();
					},error:function(data){
						console.log(data);
					}
				})
			}
		})

		$('#bayarhutang').change(function(){
			var isbayar = $(this).val();
			var hutang = $('#hutangpelanggan').val();
			var total = $('#totalpenjualan').val();
			var hutangotal = Number(hutang)+Number(total);
			if(batasTransaksi>total){
				$('#printData').prop("disabled", false);
				$('#canSubmit').val("true");
			}else{
				if(isbayar == 1){
					$('#totalpenjualan').val(hutangotal);
					$('#printData').prop("disabled", false);
					$('#canSubmit').val("true");
				}else{
					$('#printData').prop("disabled", true);
					$('#canSubmit').val("false");
				}
			}
		})

		$(document).ready(function(){
			var barang = [];
			var pelanggan = [];
			var data = {};
			data['keyword'] = "";
			updateTabelBarang();
			startTime();

			$('#notabaru').click(function(){
				var data = {};
				data['no_nota'] = $('#nonota').val();

				var con = confirm("Hapus Semua Data?");

				if(con==false)
					return false;

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>penjualan/deleteAllBarang",
			        data: data,
			        dataType: 'json',
			        success: function(data){
			            $('#id_pelanggan').val('');
			            $('#pembayaran').val('');
			            $('#totalpenjualan').val('0');
			            updateTabelBarang();
			        },error: function(data){
			        	console.log(data);
			        }
			    });
			})

			$('#namabarang').blur(function(){
				var item = $('#namabarang').val();
				var slice = item.split(" ");
				var data = {};
				if(Number(slice[slice.length-1])>0)
					data['id'] = slice[slice.length-1];
				else
					data['id'] = 0;

				if($('#satuanbarang').val()==""){
					$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>penjualan/getSatuanBarang",
			        data: data,
			        dataType: 'json',
			        success: function(data){
								var option = "<option value='1'>"+data['satuan']+"</option>";
								option += "<option value='2'>"+data['satuan2']+"</option>";
								$('#satuanbarang').empty();
								$('#satuanbarang').append(option);
								console.log(data);
			        },error: function(data){
								alert("Barang Tidak Ditemukan");
			        	console.log(data);
			        }
			    });
				}

				$('#qtybarang').focus();
			})

			$('#pembayaran').click(function(){
				$(this).focus();
			})

			$('#submitNotaPenjualan').submit(function(e){
				e.preventDefault();

				if($('#canSubmit').val()=="false")
					return false;

				var item = {};
				item['no_nota'] = $('#nonota').val();
				item['total_bayar'] = $('#totalpenjualan').val();
				item['pembayaran'] = $('#pembayaran').val();
				var pelanggan = $('#id_pelanggan').val();
				var slice = pelanggan.split(" ");

				item['id_pelanggan'] = slice[slice.length-1]

				item['waktu'] = $('#tanggal').val()+" "+$("#time").text();

				var total = $('#totalpenjualan').val();
				var bayar = $('#pembayaran').val();
				var kembali = $('#kembalian').val();

				if(Number(item['total_bayar'])>Number(item['pembayaran'])){
					if($('#id_pelanggan').val()==""){
						alert("Bon Harus isi data pelanggan");
						return false;
					}
					else
						confirmBon = confirm("Lanjutkan Transaksi Sebagai Bon?");
					item['status'] = "BON";
				}

				confirmSubmit = confirm("Simpan dan Cetak Nota?");

				if(confirmSubmit==true&&localStorage.getItem('printer')=="printer2"){
					$('#cetakTabelRecipt th:last-child, #cetakTabelRecipt td:last-child').remove();
					$('.editqtybarangfix').hide();
					$('.hidenqty').show();
					$('#cetakTabelRecipt th:nth-child(1)').remove();
					$('#cetakTabelRecipt td:nth-child(2)').remove();
					$('#addBarangTabel').remove();
					$('.colnamabarang').attr('colspan',2);

					var pelanggan = $('#id_pelanggan').val();
					var slice = pelanggan.split("-");
					// if(Number(slice[slice.length-1])>0)
					pelanggan = slice[0];

					$('#cetakTabelRecipt thead').empty();
					var thead = '<tr class="showPrint">'+
						'<td colspan="2">No Nota </td>'+
						'<td colspan="3">: <?php echo $no_nota; ?></td>'+
					'</tr>'+
					'<tr class="showPrint">'+
						'<td colspan="2">Pelanggan</td>'+
						'<td colspan="3" id="pelangganPrint">: '+pelanggan+'</td>'+
					'</tr>'+
					'<tr class="showPrint">'+
						'<td colspan="2">Waktu </td>'+
						'<td colspan="3" id="waktuPrint">: '+$('#tanggal').val()+" "+$("#time").text()+'</td>'+
					'</tr>'+
					'<tr class="showPrint">'+
						'<td colspan="5">-------------------------------------------------</td>'+
					'</tr>'+
					'<tr>'+
							'<th data-field="qty" width="13%" style="text-align:left;">Qty </th>'+
							'<th data-field="name" colspan="2" width="47%" style="text-align:left;">Nama Barang</th>'+
							'<th data-field="price" width="20%" style="text-align:center;">Harga</th>'+
							'<th data-field="price" width="20%" style="text-align:center;">Jumlah</th>'+
					'</tr>';
					$('#cetakTabelRecipt thead').append(thead);

					// $('.showPrint').show();
					// $('#pelangganPrint').text($('#id_pelanggan').val());
					// $('#waktuPrint').text($('#tanggal').val()+" "+$("#time").text());
					$('#tbody_barang').append('<td colspan="5">-------------------------------------------------</td>');
					$('#tbody_barang').append("<tr><td colspan='4'>Total</td><td style='text-align:right;'>"+total+"</td></tr>");
					$('#tbody_barang').append("<tr><td colspan='4'>Bayar</td><td style='text-align:right;'>"+bayar+"</td></tr>");
					$('#tbody_barang').append("<tr><td colspan='4'>Kembalian</td><td style='text-align:right;'>"+kembali+"</td></tr>");

					window.print();
				}else if(confirmSubmit==true&&localStorage.getItem('printer')=="printer1"){
					var isi = {};
					isi['no_nota'] = $('#nonota').val();
					isi['waktu'] = $('#tanggal').val()+" "+$("#time").text();
					var pelanggan = $('#id_pelanggan').val();
					var slice = pelanggan.split("-");
					// if(Number(slice[slice.length-1])>0)
					isi['pelanggan'] = slice[0];

					$.ajax({
				        type: "POST",
				        url: "<?=base_url()?>PrintCetak/printDotMatrix",
				        data: isi,
				        dataType: 'json',
				        success: function(data){
				        	alert(data['hasil']);
				        },error: function(data){
				        	console.log(data);
				        }
				    })
				}

				if(confirmBon==false)
					return false;

				if(confirmSubmit==false)
					return false;

				var isbayar = $("#bayarhutang").val();

				if(isbayar == 1){
					$.ajax({
				        type: "POST",
				        url: "<?=base_url()?>penjualan/SetBayarHutang",
				        data: item,
				        dataType: 'json',
				        success: function(data){

				        },error: function(data){
				        	console.log(data);
				        }
				    })
				}



				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>penjualan/submitNotaPenjualan",
			        data: item,
			        dataType: 'json',
			        success: function(data){
								alert("Berhasil Disimpan");
									location.reload();
			        },error: function(data){
			        	console.log(data);
			        }
			    })
			})

			$('#pembayaran').keyup(function(){
				var total = Number($('#totalpenjualan').val());
				var bayar = Number($('#pembayaran').val());
				var sisa = bayar-total;

				$('#kembalian').val(sisa);
			})

			$('#totalpenjualan').keyup(function(){
				var total = Number($('#totalpenjualan').val());
				var bayar = Number($('#pembayaran').val());
				var sisa = bayar-total;

				if(bayar>0)
					$('#kembalian').val(sisa);
			})

			$(document).on('click','.editBarang', function(){
				var id_barang = $(this).parent().find('.id_barang').val();
				nama_barang = $(this).closest('tr').find('td').eq(1).text();
				qty = $(this).closest('tr').find('td').eq(0).text();

				$(this).closest('tr').find('td').eq(0).empty();
				$(this).closest('tr').find('td').eq(0).append('<input type="number" id="editqtybarang" class="form-control" placeholder="Qty" required>');
				$(this).closest('tr').find('td').eq(0).find('#editqtybarang').val(qty);
				$(this).closest('tr').find('td').eq(0).find('#editqtybarang').focus();

				$(this).closest('tr').find('td').eq(5).find('.editBarang').hide();
				$(this).closest('tr').find('td').eq(5).find('.deleteBarang').hide();
				$(this).closest('tr').find('td').eq(5).find('.submitEdit').show();
				$(this).closest('tr').find('td').eq(5).find('.cancelEdit').show();

				$('#editnamabarang').focus();
			})

			$(document).on('click','.cancelEdit', function(){
				$(this).closest('tr').find('td').eq(5).find('.editBarang').hide();
				$(this).closest('tr').find('td').eq(5).find('.deleteBarang').hide();
				$(this).closest('tr').find('td').eq(5).find('.submitEdit').show();
				$(this).closest('tr').find('td').eq(5).find('.cancelEdit').show();

				// $(this).closest('tr').find('td').eq(1).text(nama_barang);
				$(this).closest('tr').find('td').eq(3).text(qty);
			})

			$(document).on('keypress','.editqtybarangfix', function(e){
				if(e.which == 13){
					var data = {};
					data['id'] = $(this).closest('tr').find('td').eq(5).find('.id_barang').val();
					data['qty'] = $(this).val();

					console.log(data);

					var con = confirm("Edit Data Barang?");

					if(con==false)
						return false;

					$.ajax({
				        type: "POST",
				        url: "<?=base_url()?>penjualan/editDataBarang",
				        data: data,
				        dataType: 'json',
				        success: function(data){
				            alert(data['message']);
							$('#submitBarang').show();
				            updateTabelBarang();
				        },error: function(data){
				        	console.log(data);
				        }
				    });
				}
			})

			$(document).on('click','.submitEdit', function(){
				var data = {};
				data['id'] = $(this).parent().find('.id_barang').val();
				data['qty'] = $('#editqtybarang').val();

				alert($('#editqtybarang').val());

				console.log(data);

				var con = confirm("Edit Data Barang?");

				if(con==false)
					return false;

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>penjualan/editDataBarang",
			        data: data,
			        dataType: 'json',
			        success: function(data){
			            alert(data['message']);
						$('#submitBarang').show();
			            updateTabelBarang();
			        },error: function(data){
			        	console.log(data);
			        }
			    });
			})

			$(document).on('click','.deleteBarang', function(){
				var data = {};
				data['id_barang'] = $(this).parent().find('.id_barang').val();

				var con = confirm("Hapus Data?");

				if(con==false)
					return false;

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>penjualan/deleteDataBarang",
			        data: data,
			        dataType: 'json',
			        success: function(data){
			            alert(data['message']);
			            updateTabelBarang();
			        },error: function(data){
			        	console.log(data);
			        }
			    });
			})

			$('#submitBarang').submit(function(e){
				e.preventDefault();
				var item = $('#namabarang').val();
				var slice = item.split(" ");
				var item = {};
				if(Number(slice[slice.length-1])>0)
					item['id'] = slice[slice.length-1];
				else
					item['id'] = 0;
				item['no_nota'] = $('#nonota').val();
				item['qty'] = $('#qtybarang').val();
				item['satuan'] = $('#satuanbarang').val();

				$.ajax({
			        type: "POST",
			        url: "<?=base_url()?>penjualan/submitDataBarang",
			        data: item,
			        dataType: 'json',
			        success: function(data){
									console.log(data['message']);
									if(data['message']!=undefined) alert(data['message']);
			            $('#namabarang').val("");
			            $('#qtybarang').val("");
									$('#namabarang').focus();
			            updateTabelBarang();
			        },error:function(data){
			        	console.log(data);
			        }
			    });
			})

			$.ajax({
		        type: "POST",
		        url: "<?=base_url()?>masterbarang/searchDataBarang",
		        data: data,
		        dataType: 'json',
		        success: function(data){
		            for(var i= 0; i<data.length;i++){
		            	barang[i] = data[i]['nama_barang']+" - "+data[i]['id_barang'];
		            }
		        }
		    });

		    $.ajax({
		        type: "POST",
		        url: "<?=base_url()?>masterpelanggan/searchDataPelanggan",
		        data: data,
		        dataType: 'json',
		        success: function(data){
		            for(var i= 0; i<data.length;i++){
		            	pelanggan[i] = data[i]['nama']+" - "+data[i]['id_pelanggan'];
		            }
		        },error:function(data){
							console.log(data);
						}
		    });

			$( ".typeahead" ).autocomplete({
	          	source: barang
	        });

      $( ".typeaheadp" ).autocomplete({
        	source: pelanggan
      });

			/*$('#printData').click(function(){
					var total = $('#totalpenjualan').val();
					var bayar = $('#pembayaran').val();
					var kembali = $('#kembalian').val();

					// confirmSubmit = confirm("Simpan dan Cetak Nota?");

					if(Number(item['total_bayar'])>Number(item['pembayaran'])){
						confirmBon = confirm("Lanjutkan Transaksi Sebagai Bon?");
					}

					if(confirmSubmit==true){
						$('#cetakTabelRecipt th:last-child, #cetakTabelRecipt td:last-child').remove();
						$('#cetakTabelRecipt th:first-child, #cetakTabelRecipt td:first-child').remove();
						$('#addBarangTabel').remove();

						var pelanggan = $('#id_pelanggan').val();
						var slice = pelanggan.split("-");
						// if(Number(slice[slice.length-1])>0)
						pelanggan = slice[0];

						$('#cetakTabelRecipt thead').empty();
						var thead = '<tr class="showPrint">'+
							'<td colspan="1">No Nota </td>'+
							'<td colspan="3">: <?php echo $no_nota; ?></td>'+
						'</tr>'+
						'<tr class="showPrint">'+
							'<td colspan="1">Nama Pelanggan </td>'+
							'<td colspan="3" id="pelangganPrint">: '+pelanggan+'</td>'+
						'</tr>'+
						'<tr class="showPrint">'+
							'<td colspan="1">Waktu </td>'+
							'<td colspan="3" id="waktuPrint">: '+$('#tanggal').val()+" "+$("#time").text()+'</td>'+
						'</tr>'+
						'<tr class="showPrint">'+
							'<td colspan="4">------------------------------------------------------------------------------------------------</td>'+
						'</tr>'+
						'<tr>'+
							'<th data-field="name" width="35%" style="text-align:left;">Nama Barang</th>'+
								'<th data-field="qty" width="10%" style="text-align:center;">Qty</th>'+
								'<th data-field="price" width="20%" style="text-align:center;">Harga</th>'+
								'<th data-field="price" width="20%" style="text-align:center;">Jumlah</th>'+
						'</tr>';
						$('#cetakTabelRecipt thead').append(thead);

						// $('.showPrint').show();
						// $('#pelangganPrint').text($('#id_pelanggan').val());
						// $('#waktuPrint').text($('#tanggal').val()+" "+$("#time").text());
						$('#tbody_barang').append('<td colspan="4">------------------------------------------------------------------------------------------------</td>');
						$('#tbody_barang').append("<tr><td colspan='3'>Total</td><td style='text-align:right;'>"+total+"</td></tr>");
						$('#tbody_barang').append("<tr><td colspan='3'>Bayar</td><td style='text-align:right;'>"+bayar+"</td></tr>");
						$('#tbody_barang').append("<tr><td colspan='3'>Kembalian</td><td style='text-align:right;'>"+kembali+"</td></tr>");

						window.print();
					}
			})*/

			$('#namabarang').change(function(){
				updateBarangAdd();
			})

			$('#qtybarang').keyup(function(){
				updateBarangAdd();
			})

			$('#satuanbarang').change(function(){
				updateBarangAdd();
			})

		})

		function updateBarangAdd(){
				if($('#namabarang').val()=="" && $('#qtybarang').val()=="" && $('#satuanbarang').val()==""){}
				else{
					var item = $('#namabarang').val();
					var slice = item.split(" ");
					var item = {};
					item['id'] = slice[slice.length-1];
					var qty = $('#qtybarang').val();
					var satuan = $('#satuanbarang').val();

					$.ajax({
				        type: "POST",
				        url: "<?=base_url()?>penjualan/getDataBarangId",
				        data: item,
				        dataType: 'json',
				        success: function(data){
										var jumlah = 0;
				            if(satuan==1){
											$('#hargabarang').val(data['harga_jual']);
											jumlah = Number(data['harga_jual'])*Number(qty);
										}
										else{
											$('#hargabarang').val(data['harga_jual2']);
											jumlah = Number(data['harga_jual2'])*Number(qty);
										}
										$('#jumlahbarang').val(jumlah);
				        },error:function(data){
									console.log(data);
								}
				    });
				}
		}

	function updateTabelBarang(){
		var data = {};
		data['no_nota'] = $('#nonota').val();
		$.ajax({
	        type: "POST",
	        url: "<?=base_url()?>penjualan/searchDataBarang",
	        data: data,
	        dataType: 'json',
	        success: function(data){
	            if(data.length>0){
	            	$('#tbody_barang').empty();
	            	var total = 0;
	            	for(var i = 0; i<data.length; i++){
	            		var jumlah = Number(data[i]['qty'])*Number(data[i]['harga_jual']);
	            		total+=jumlah;
	            		$('#tbody_barang').append(
	            			'<tr>'
								+'<td style="text-align:left; width:20px;"><input type="number" class="form-control editqtybarangfix" style="height:30px;" placeholder="Qty" value="'+data[i]['qty']+'" required> <span class="hidenqty" style="display:none;">'+data[i]['qty']+'</span></td>'
								+'<td>'+data[i]['satuan']+'</td>'
						        +'<td class="colnamabarang">'+data[i]['nama_barang']+'</td>'
						        +'<td style="text-align:right;">'+data[i]['harga_jual']+'</td>'
						        +'<td style="text-align:right;">'+jumlah+'</td>'
						        +'<td><center>'
						        	+'<input type="hidden" class="id_barang" value="'+data[i]['no_nota_d']+'">'
						        	// +'<button class="btn btn-warning  btn-glyp editBarang"><span class="glyphicon glyphicon-edit"></span></button>'
						        	+'<button class="btn btn-danger  btn-glyp deleteBarang"><span class="glyphicon glyphicon-trash"></span></button>'
											+'<span class="glyphicon glyphicon-ok btn btn-glyp btn-success submitEdit" style="display:none; ""></span>'
											+'<span class="glyphicon glyphicon-remove btn btn-glyp btn-info cancelEdit" style="display:none;"></span>'
						        +'</center></td>'
						    +'</tr>'
	            		);
	            	}
	            	$('#totalpenjualan').val(total);
								var isbayar = $("#bayarhutang").val();
								var hutang = $('#hutangpelanggan').val();
								var total = $('#totalpenjualan').val();
								var hutangotal = Number(hutang)+Number(total);
								if(isbayar == 1){
									$('#totalpenjualan').val(hutangotal);
									$('#printData').prop("disabled", false);
								}

								if(Number(hutang)>0&&Number(total)>batasTransaksi)
									$('#printData').prop("disabled", true);
	            }else{
	            	$('#tbody_barang').empty();
	            	$('#tbody_barang').append('<tr><td colspan="7"><center>Tidak Ada Data Barang</center></td></tr>');
	            }
	        }
	    });
	}

	function checkTime(i) {
        return (i < 10) ? "0" + i : i;
    }

    function startTime() {
        var today = new Date(),
            h = checkTime(today.getHours()),
            m = checkTime(today.getMinutes()),
            s = checkTime(today.getSeconds());
            d = checkTime(today.getDate());
            mt = checkTime(today.getMonth()+1);
            y = today.getFullYear();
        $('#tanggal').val(y+"-"+mt+"-"+d);
        document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
        t = setTimeout(function () {
            startTime()
        }, 500);
    }

		function cetakNota(){
			$('#cetakTabelRecipt th:last-child, #cetakTabelRecipt td:last-child').remove();
			$('.editqtybarangfix').hide();
			$('.hidenqty').show();
			$('#cetakTabelRecipt th:nth-child(1)').remove();
			$('#cetakTabelRecipt td:nth-child(2)').remove();
			$('#addBarangTabel').remove();

			var pelanggan = $('#id_pelanggan').val();
			var slice = pelanggan.split("-");
			// if(Number(slice[slice.length-1])>0)
			pelanggan = slice[0];

			$('#cetakTabelRecipt thead').empty();
			var thead = '<tr class="showPrint">'+
				'<td colspan="1">No Nota </td>'+
				'<td colspan="3">: <?php echo $no_nota; ?></td>'+
			'</tr>'+
			'<tr class="showPrint">'+
				'<td colspan="1">Nama Pelanggan </td>'+
				'<td colspan="3" id="pelangganPrint">: '+pelanggan+'</td>'+
			'</tr>'+
			'<tr class="showPrint">'+
				'<td colspan="1">Waktu </td>'+
				'<td colspan="3" id="waktuPrint">: '+$('#tanggal').val()+" "+$("#time").text()+'</td>'+
			'</tr>'+
			'<tr class="showPrint">'+
				'<td colspan="4">------------------------------------------------------------------------</td>'+
			'</tr>'+
			'<tr>'+
					'<th data-field="qty" width="10%" style="text-align:center;">Qty</th>'+
					'<th data-field="name" width="35%" style="text-align:left;">Nama Barang</th>'+
					'<th data-field="price" width="20%" style="text-align:center;">Harga</th>'+
					'<th data-field="price" width="20%" style="text-align:center;">Jumlah</th>'+
			'</tr>';
			$('#cetakTabelRecipt thead').append(thead);

			// $('.showPrint').show();
			// $('#pelangganPrint').text($('#id_pelanggan').val());
			// $('#waktuPrint').text($('#tanggal').val()+" "+$("#time").text());
			$('#tbody_barang').append('<td colspan="4">------------------------------------------------------------------------</td>');
			$('#tbody_barang').append("<tr><td colspan='3'>Total</td><td style='text-align:right;'>"+total+"</td></tr>");
			$('#tbody_barang').append("<tr><td colspan='3'>Bayar</td><td style='text-align:right;'>"+bayar+"</td></tr>");
			$('#tbody_barang').append("<tr><td colspan='3'>Kembalian</td><td style='text-align:right;'>"+kembali+"</td></tr>");

			window.print();
		}

	</script>
</body>

</html>
