<?php
class m_masterbarang extends CI_Model{
	public function getDataBarang(){
		$query = $this->db->query("SELECT * FROM master_barang");
		return $query->result_array();
	}

	public function getSearchDataBarang($keyword){
		$query = $this->db->query("SELECT * FROM master_barang WHERE nama_barang LIKE '%$keyword%'");
		return $query->result_array();
	}

	public function insertDatabarang($data){
		$result = $this->db->insert('master_barang', $data);
		return $result;
	}

	public function deleteDataBarang($id){
		$this->db->where('id_barang', $id);
  		$result = $this->db->delete('master_barang');
  		return $result;
	}

	public function updateDataBarang($data, $id){
		$this->db->where('id_barang', $id);
    	$result = $this->db->update("master_barang", $data);
    	return $result;
	}
}
?>