<?php
class m_penjualan extends CI_Model{
	public function getDataBarangId($id){
		$query = $this->db->query("SELECT * FROM master_barang WHERE id_barang = $id");
		return $query->row_array();
	}

	public function getNomerNota(){
		$query = $this->db->query("SELECT MAX(no_nota) as no_nota FROM nota_penjualan_m");
		return $query->row_array();
	}

	public function insertDatabarang($data){
		$result = $this->db->insert('nota_penjualan_d', $data);
		return $result;
	}

	public function insertNotaPenjualan($data){
		$result = $this->db->insert('nota_penjualan_m', $data);
		return $result;
	}

	public function getSearchDataBarang($id){
		$query = $this->db->query("SELECT * FROM nota_penjualan_d WHERE nomor_nota = $id");
		return $query->result_array();
	}

	public function deleteDataBarang($id){
		$this->db->where('no_nota_d', $id);
  		$result = $this->db->delete('nota_penjualan_d');
  		return $result;
	}

	public function deleteAllBarang($id){
		$this->db->where('nomor_nota', $id);
  		$result = $this->db->delete('nota_penjualan_d');
  		return $result;
	}

	public function updateDataBarang($data, $id){
		$this->db->where('no_nota_d', $id);
    	$result = $this->db->update("nota_penjualan_d", $data);
    	return $result;
	}

	public function getSatuanBarang($id){
		$query = $this->db->query("SELECT * FROM master_barang WHERE id_barang = $id");
		return $query->row_array();
	}

	public function getHutangPelanggan($id){
		$query = $this->db->query("SELECT n.id_pelanggan, sum(total_bayar-pembayaran) as hutang, batas_transaksi FROM nota_penjualan_m n INNER JOIN master_pelanggan m ON n.id_pelanggan = m.id_pelanggan WHERE n.status = 'BON' AND n.id_pelanggan = '$id' GROUP BY n.id_pelanggan LIMIT 1");
		return $query->row_array();
	}

	public function SetBayarHutang($id,$tgl){
		return $this->db->query("UPDATE `nota_penjualan_m` SET `status` = 'LUNAS', waktu_pelunasan = '$tgl' WHERE `nota_penjualan_m`.`id_pelanggan` = '$id'");
	}
}
?>
