<?php
class m_masterpelanggan extends CI_Model{
	public function getDataPelanggan(){
		$query = $this->db->query("SELECT * FROM master_pelanggan");
		return $query->result_array();
	}

	public function getSearchDataPelanggan($keyword){
		$query = $this->db->query("SELECT * FROM master_pelanggan WHERE nama LIKE '%$keyword%'");
		return $query->result_array();
	}

	public function insertDataPelanggan($data){
		$result = $this->db->insert('master_pelanggan', $data);
		return $result;
	}

	public function deleteDataPelanggan($id){
		$this->db->where('id_pelanggan', $id);
  		$result = $this->db->delete('master_pelanggan');
  		return $result;
	}

	public function updateDataPelanggan($data, $id){
		$this->db->where('id_pelanggan', $id);
    	$result = $this->db->update("master_pelanggan", $data);
    	return $result;
	}
}
?>