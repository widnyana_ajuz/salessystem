-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 31, 2016 at 02:28 
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sipfix`
--

-- --------------------------------------------------------

--
-- Table structure for table `master_barang`
--

CREATE TABLE `master_barang` (
  `id_barang` int(11) NOT NULL,
  `nama_barang` varchar(128) NOT NULL,
  `satuan` varchar(64) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `satuan2` varchar(100) NOT NULL,
  `harga_jual2` int(11) NOT NULL,
  `konversi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_barang`
--

INSERT INTO `master_barang` (`id_barang`, `nama_barang`, `satuan`, `harga_beli`, `harga_jual`, `satuan2`, `harga_jual2`, `konversi`) VALUES
(1, 'Tissue Nice 100 Gram', 'Buah', 5000, 5800, 'Dus', 55000, 2),
(2, 'Roti Maryam Rasa Coklat', 'Buah', 4300, 5000, 'Dus', 46700, 0),
(3, 'Teh Melati Celup', 'Buah', 0, 4500, 'Dus', 34500, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_pelanggan`
--

CREATE TABLE `master_pelanggan` (
  `id_pelanggan` int(11) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `status` varchar(32) DEFAULT NULL,
  `alamat` text,
  `telepon` varchar(32) DEFAULT NULL,
  `batas_transaksi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_pelanggan`
--

INSERT INTO `master_pelanggan` (`id_pelanggan`, `nama`, `status`, `alamat`, `telepon`, `batas_transaksi`) VALUES
(1, 'Putu Widnyana Santika', 'Sdr', 'Jalan Tunjung Baru Yogya', '085799168532', 50000),
(2, 'Setiawan Prawira', 'Sdr', 'Padang Luwih', '08578921839', 0),
(4, 'Wayan Siaga', 'Sdr', 'Tunjung Baru', '0834587878', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nota_penjualan_d`
--

CREATE TABLE `nota_penjualan_d` (
  `no_nota_d` int(11) NOT NULL,
  `nomor_nota` varchar(32) NOT NULL,
  `nama_barang` varchar(128) NOT NULL,
  `satuan` varchar(64) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `satuan2` varchar(100) NOT NULL,
  `harga_jual2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nota_penjualan_d`
--

INSERT INTO `nota_penjualan_d` (`no_nota_d`, `nomor_nota`, `nama_barang`, `satuan`, `harga_beli`, `harga_jual`, `qty`, `satuan2`, `harga_jual2`) VALUES
(1, '001', 'Tissue Nice 100 Gram', 'Buah', 5000, 5800, 3, '', 0),
(3, '001', 'Roti Maryam Rasa Coklat', 'Buah', 4300, 5000, 15, '', 0),
(4, '003', 'Roti Maryam Rasa Coklat', 'Buah', 4300, 5000, 5, '', 0),
(5, '004', 'Teh Melati Celup', 'Buah', 0, 4500, 3, '', 0),
(6, '005', 'Tissue Nice 100 Gram', 'Buah', 5000, 5800, 5, '', 0),
(9, '005', 'Roti Maryam Rasa Coklat', 'Buah', 4300, 5000, 2, '', 0),
(10, '005', 'Teh Melati Celup', 'Buah', 0, 4500, 4, '', 0),
(11, '006', 'Tissue Nice 100 Gram', 'Buah', 5000, 5800, 4, '', 0),
(12, '007', 'Tissue Nice 100 Gram', 'Buah', 5000, 5800, 4, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `nota_penjualan_m`
--

CREATE TABLE `nota_penjualan_m` (
  `no_nota` varchar(32) NOT NULL,
  `id_pelanggan` int(11) DEFAULT NULL,
  `waktu` datetime NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `pembayaran` int(11) NOT NULL,
  `status` enum('LUNAS','BON') NOT NULL DEFAULT 'LUNAS',
  `waktu_pelunasan` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nota_penjualan_m`
--

INSERT INTO `nota_penjualan_m` (`no_nota`, `id_pelanggan`, `waktu`, `total_bayar`, `pembayaran`, `status`, `waktu_pelunasan`) VALUES
('001', 1, '2016-08-14 13:44:16', 92400, 100000, 'LUNAS', '2016-10-29 20:09:20'),
('002', NULL, '2016-08-14 13:44:16', 92400, 100000, 'LUNAS', '0000-00-00 00:00:00'),
('003', 1, '2016-10-29 18:03:15', 25000, 25000, 'LUNAS', '2016-10-29 20:09:20'),
('004', 1, '2016-10-29 18:09:49', 13500, 5000, 'LUNAS', '2016-10-29 20:09:20'),
('005', 1, '2016-10-29 20:09:20', 65500, 70000, 'LUNAS', '2016-10-29 20:09:20'),
('006', 1, '2016-10-29 20:22:19', 23200, 25000, 'LUNAS', '0000-00-00 00:00:00'),
('007', 2, '2016-10-29 20:23:47', 23200, 25000, 'LUNAS', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`) VALUES
(1, 'admin', '14c9c680b61b8aa0f591a51367eabf9b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_barang`
--
ALTER TABLE `master_barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `master_pelanggan`
--
ALTER TABLE `master_pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`);

--
-- Indexes for table `nota_penjualan_d`
--
ALTER TABLE `nota_penjualan_d`
  ADD PRIMARY KEY (`no_nota_d`);

--
-- Indexes for table `nota_penjualan_m`
--
ALTER TABLE `nota_penjualan_m`
  ADD PRIMARY KEY (`no_nota`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `master_barang`
--
ALTER TABLE `master_barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `master_pelanggan`
--
ALTER TABLE `master_pelanggan`
  MODIFY `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `nota_penjualan_d`
--
ALTER TABLE `nota_penjualan_d`
  MODIFY `no_nota_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
